#ifndef __PNP_GEOMETRY_H__
#define __PNP_GEOMETRY_H__

#include "types.h"

void pnp_intristics_init(pnp_intristics_t *, double, double, double, double, double, double, double, double, double);
void pnp_intristics_K(pnp_intristics_t *, double [3][3]);
void pnp_intristics_K_inv(pnp_intristics_t *, double [3][3]);

void pnp_extrinsics_init(pnp_extrinsics_t *, double, double, double, double [3][3]);
void pnp_extrinsics_init_euler(pnp_extrinsics_t *, double, double, double, double, double, double);
int pnp_extrinsics_calc(pnp_point3_t [4], pnp_point3_t [4], pnp_extrinsics_t *);

int pnp_homography(pnp_point2_t [4], pnp_point2_t [4], double [3][3]);

void pnp_dcm_zyx_to_euler(double [3][3], double [3]);
void pnp_euler_to_dcm_zyx(double, double, double, double [3][3]);

void pnp_project_points_tr(pnp_point3_t [], int, pnp_intristics_t *, pnp_extrinsics_t *, pnp_point2_t []);
void pnp_project_points_rt(pnp_point3_t [], int, pnp_intristics_t *, pnp_extrinsics_t *, pnp_point2_t []);

#endif
