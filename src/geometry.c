#include <math.h>
#include <string.h>

#include "geometry.h"
#include "linalg.h"

void pnp_intristics_init(pnp_intristics_t * intristics, double fx, double fy, double x0, double y0, double k1, double k2, double k3, double p1, double p2) {
	intristics->fx = fx;
	intristics->fx_i = 1 / fx;
	intristics->fy = fy;
	intristics->fy_i = 1 / fy;

	intristics->x0 = x0;
	intristics->x0_i = 1 / x0;
	intristics->y0 = y0;
	intristics->y0_i = 1 / y0;

	intristics->x0_fx_i = x0 / fx;
	intristics->y0_fy_i = y0 / fy;

	intristics->k1 = k1;
	intristics->k2 = k2;
	intristics->k3 = k3;
	intristics->p1 = p1;
	intristics->p2 = p2;
}

void pnp_intristics_K(pnp_intristics_t * intristics, double K[3][3]) {
	K[0][0] = intristics->fx;
	K[0][1] = 0;
	K[0][2] = intristics->x0;

	K[1][0] = 0;
	K[1][1] = intristics->fy;
	K[1][2] = intristics->y0;

	K[2][0] = 0;
	K[2][1] = 0;
	K[2][2] = 1;
}

void pnp_intristics_K_inv(pnp_intristics_t * intristics, double K_inv[3][3]) {
	K_inv[0][0] = intristics->fx_i;
	K_inv[0][1] = 0;
	K_inv[0][2] = -intristics->x0_fx_i;

	K_inv[1][0] = 0;
	K_inv[1][1] = intristics->fy_i;
	K_inv[1][2] = -intristics->y0_fy_i;

	K_inv[2][0] = 0;
	K_inv[2][1] = 0;
	K_inv[2][2] = 1;
}


void pnp_extrinsics_init(pnp_extrinsics_t * extrinsics, double x, double y, double z, double R[3][3]) {
	extrinsics->x = x;
	extrinsics->y = y;
	extrinsics->z = z;

	memcpy(extrinsics->R, R, 9 * sizeof(double));
}

void pnp_extrinsics_init_euler(pnp_extrinsics_t * extrinsics, double x, double y, double z, double omega, double phi, double kappa) {
	extrinsics->x = x;
	extrinsics->y = y;
	extrinsics->z = z;

	pnp_euler_to_dcm_zyx(omega, phi, kappa, extrinsics->R);
}

int pnp_extrinsics_calc(pnp_point3_t obj[3], pnp_point3_t obj_c[3], pnp_extrinsics_t * extrinsics) {
	double a[9][9], b[9], tmp[9];
	memset(a, 0, sizeof(double) 9 * 9);
	
	a[0][0] = obj[0].x; a[0][1] = obj[0].y; a[0][2] = 1.;
	a[1][0] = obj[1].x; a[1][1] = obj[1].y; a[1][2] = 1.;
	a[2][0] = obj[2].x; a[2][1] = obj[2].y; a[2][2] = 1.;
	
	a[3][3] = obj[0].x; a[3][4] = obj[0].y; a[3][5] = 1.;
	a[4][3] = obj[1].x; a[4][4] = obj[1].y; a[4][5] = 1.;
	a[5][3] = obj[2].x; a[5][4] = obj[2].y; a[5][5] = 1.;

	a[6][6] = obj[0].x; a[6][7] = obj[0].y; a[6][8] = 1.;
	a[7][6] = obj[1].x; a[7][7] = obj[1].y; a[7][8] = 1.;
	a[8][6] = obj[2].x; a[8][7] = obj[2].y; a[8][8] = 1.;

	b[0] = obj_c[0].x; b[1] = obj_c[1].x; b[2] = obj_c[2].x;
	b[3] = obj_c[0].y; b[4] = obj_c[1].y; b[5] = obj_c[2].y;
	b[6] = obj_c[0].z; b[7] = obj_c[1].z; b[8] = obj_c[2].z;

	if(pnp_la_solven((double *)a, b, 9, tmp)) return -1;
	
	extrinsics->R[0][0] = b[0]; extrinsics->R[0][1] = b[1];
	extrinsics->R[1][0] = b[3]; extrinsics->R[1][1] = b[4];
	extrinsics->R[2][0] = b[6]; extrinsics->R[2][1] = b[7];

	extrinsics->R[0][2] = extrinsics->R[1][0] * extrinsics->R[2][1] - extrinsics->R[2][0] * extrinsics->R[1][1];
	extrinsics->R[1][2] = extrinsics->R[2][0] * extrinsics->R[0][1] - extrinsics->R[0][0] * extrinsics->R[2][1];
	extrinsics->R[2][2] = extrinsics->R[0][0] * extrinsics->R[1][1] - extrinsics->R[1][0] * extrinsics->R[0][1];

	extrinsics->x = -(extrinsics->R[0][0] * b[2] + extrinsics->R[1][0] * b[5] + extrinsics->R[2][0] * b[8]);
	extrinsics->y = -(extrinsics->R[0][1] * b[2] + extrinsics->R[1][1] * b[5] + extrinsics->R[2][1] * b[8]);
	extrinsics->z = -(extrinsics->R[0][2] * b[2] + extrinsics->R[1][2] * b[5] + extrinsics->R[2][2] * b[8]);

	return 0;
}

/*
 * Fischler Martin, Bolles Robert -
 * Random Sample Consensus: A Paradigm for Model Fitting with Applications to Image Analysis and Automated Cartography
 * appendix B, page 395
 */
int pnp_homography(pnp_point2_t sourcePoints[4], pnp_point2_t destinationPoints[4], double homography[3][3]) {
	double Q[3][3] = {
		{ sourcePoints[0].x, sourcePoints[0].y, 1 },
		{ sourcePoints[1].x, sourcePoints[1].y, 1 },
		{ sourcePoints[2].x, sourcePoints[2].y, 1 }
	};

	double P[3][3] = {
		{ destinationPoints[0].x, destinationPoints[0].y, 1 },
		{ destinationPoints[1].x, destinationPoints[1].y, 1 },
		{ destinationPoints[2].x, destinationPoints[2].y, 1 }
	};

	double Qinv[3][3], Pinv[3][3];
	double V[3], R[3];
	double w1, w2;

	if(pnp_la_inv3(Q, Qinv)) return 1;
	if(pnp_la_inv3(P, Pinv)) return 1;

	R[0] = Qinv[0][0] * sourcePoints[3].x + Qinv[1][0] * sourcePoints[3].y + Qinv[2][0];
	R[1] = Qinv[0][1] * sourcePoints[3].x + Qinv[1][1] * sourcePoints[3].y + Qinv[2][1];
	R[2] = Qinv[0][2] * sourcePoints[3].x + Qinv[1][2] * sourcePoints[3].y + Qinv[2][2];

	V[0] = Pinv[0][0] * destinationPoints[3].x + Pinv[1][0] * destinationPoints[3].y + Pinv[2][0];
	V[1] = Pinv[0][1] * destinationPoints[3].x + Pinv[1][1] * destinationPoints[3].y + Pinv[2][1];
	V[2] = Pinv[0][2] * destinationPoints[3].x + Pinv[1][2] * destinationPoints[3].y + Pinv[2][2];

	w1 = (V[0] / R[0]) * (R[2] / V[2]);
	w2 = (V[1] / R[1]) * (R[2] / V[2]);

	/* returns non-transposed matrix (opposite to method presented in paper) */

	homography[0][0] = Qinv[0][0] * w1 * P[0][0] + Qinv[0][1] * w2 * P[1][0] + Qinv[0][2] * P[2][0];
	homography[1][0] = Qinv[0][0] * w1 * P[0][1] + Qinv[0][1] * w2 * P[1][1] + Qinv[0][2] * P[2][1];
	homography[2][0] = Qinv[0][0] * w1 * P[0][2] + Qinv[0][1] * w2 * P[1][2] + Qinv[0][2] * P[2][2];

	homography[0][1] = Qinv[1][0] * w1 * P[0][0] + Qinv[1][1] * w2 * P[1][0] + Qinv[1][2] * P[2][0];
	homography[1][1] = Qinv[1][0] * w1 * P[0][1] + Qinv[1][1] * w2 * P[1][1] + Qinv[1][2] * P[2][1];
	homography[2][1] = Qinv[1][0] * w1 * P[0][2] + Qinv[1][1] * w2 * P[1][2] + Qinv[1][2] * P[2][2];

	homography[0][2] = Qinv[2][0] * w1 * P[0][0] + Qinv[2][1] * w2 * P[1][0] + Qinv[2][2] * P[2][0];
	homography[1][2] = Qinv[2][0] * w1 * P[0][1] + Qinv[2][1] * w2 * P[1][1] + Qinv[2][2] * P[2][1];
	homography[2][2] = Qinv[2][0] * w1 * P[0][2] + Qinv[2][1] * w2 * P[1][2] + Qinv[2][2] * P[2][2];
	
	return 0;	
}

void pnp_dcm_zyx_to_euler(double R[3][3], double euler[3]) {
	euler[0] = atan2(R[2][1], R[2][2]);
	euler[1] = atan2(-R[2][0], sqrt(R[2][0] * R[2][0] + R[2][1] * R[2][1]));
	euler[2] = atan2(R[1][0], R[0][0]);
}

void pnp_euler_to_dcm_zyx(double omega, double phi, double kappa, double R[3][3]) {
	double sin_o, cos_o, sin_p, cos_p, sin_k, cos_k;

	sin_o = sin(omega);
	cos_o = cos(omega);
	
	sin_p = sin(phi);
	cos_p = cos(phi);
	
	sin_k = sin(kappa);
	cos_k = cos(kappa);

	 R[0][0] = cos_p * cos_k;
	 R[0][1] = cos_k * sin_o * sin_p - cos_o * sin_k;
	 R[0][2] = cos_k * cos_o * sin_p + sin_o * sin_k;
	 R[1][0] = cos_p * sin_k;
	 R[1][1] = cos_o * cos_k + sin_o * sin_p * sin_k;
	 R[1][2] = -cos_k * sin_o + cos_o * sin_p * sin_k;
	 R[2][0] = -sin_p;
	 R[2][1] = cos_p * sin_o;
	 R[2][2] = cos_o * cos_p;
}

void pnp_project_points_tr(pnp_point3_t object[], int n, pnp_intristics_t * intristics, pnp_extrinsics_t * extrinsics, pnp_point2_t image[]) {
	int i;
	double K[3][3], KR[3][3], obj_c[3], tmp[3];
	
	pnp_intristics_K(intristics, K);
	pnp_la_mul_3333(K, extrinsics->R, KR);

	for(i = 0; i < n; i++) {
		obj_c[0] = object[i].x - extrinsics->x;
		obj_c[1] = object[i].y - extrinsics->y;
		obj_c[2] = object[i].z - extrinsics->z;

		pnp_la_mul_3331(KR, obj_c, tmp);

		image[i].x = tmp[0] / tmp[2];
		image[i].y = tmp[1] / tmp[2];
	}
}


void pnp_project_points_rt(pnp_point3_t object[], int n, pnp_intristics_t * intristics, pnp_extrinsics_t * extrinsics, pnp_point2_t image[]) {
	int i;
	double tmp;

	for(i = 0; i < n; i++) {
		tmp = extrinsics->R[2][0] * object[i].x + extrinsics->R[2][1] * object[i].y + extrinsics->R[2][2] * object[i].z + extrinsics->z;
	
		image[i].x = (extrinsics->R[0][0] * object[i].x + extrinsics->R[0][1] * object[i].y + extrinsics->R[0][2] * object[i].z + extrinsics->x) / tmp;
		image[i].y = (extrinsics->R[1][0] * object[i].x + extrinsics->R[1][1] * object[i].y + extrinsics->R[1][2] * object[i].z + extrinsics->y) / tmp;
	}
}
