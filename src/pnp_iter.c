#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "pnp_iter.h"
#include "geometry.h"
#include "linalg.h"

// remove
#include <stdio.h>

int pnp_iter_church(pnp_intristics_t * intristics, pnp_point2_t image[], pnp_point3_t object[], int n, pnp_gauss_newton_params_t * gn, pnp_extrinsics_t * extrinsics) {
#define J_IDX(i, j) J[((i) * 3 + (j))]
	/* result of procedure */
	int res = -1;
	/* auxiliary variables for performing iterations */
	int iter = 0, i, j, k;
	const int comb = (n * (n - 1)) / 2;
	/* auxiliary variables for calculations */
	double error, error_min = gn->error_max, error_max = gn->error_min, cos_AB;
	/* matrices */
	double A[3][3], JT_f[3], A_inv[3][3], tmp[3], K_inv[3][3];
	/* mallocs */
	double * cos_ab = NULL, * lengths = NULL, * f = NULL, * J = NULL;
	pnp_point3_t * image_n = NULL, * object_n = NULL;

	image_n = malloc(sizeof(pnp_point3_t) * n);
	object_n = malloc(sizeof(pnp_point3_t) * n);
	cos_ab = malloc(sizeof(double) * comb);
	lengths = malloc(sizeof(double) * comb);
	f = malloc(sizeof(double) * comb);
	J = malloc(sizeof(double) * comb * 3);

	if(!image_n || !object_n || !cos_ab || !lengths || !f || !J) goto pnp_iter_church_end;

	/* precomputing cosines of angles between image points */
	pnp_intristics_K_inv(intristics, K_inv);
	
	for(i = 0; i < n; i++) {
		image_n[i].x = (image[i].x - intristics->x0) * intristics->fx_i;
		image_n[i].y = (image[i].y - intristics->y0) * intristics->fy_i; 
		image_n[i].z = 1;
		pnp_la_normalize3_p(&image_n[i]);
	}
	
	for(k = 0, i = 0; i < n; i++) {
		for(j = i + 1; j < n; j++, k++) {
			cos_ab[k] = pnp_la_dot3_p(&image_n[i], &image_n[j]);
		}
	}

	/* minimizing Church's condition error */

	error = gn->error_max;
	for(iter = 0; iter < gn->iterations && error > gn->error_min; iter++) {
		/* precomputing normalized object points */
		for(i = 0; i < n; i++) {
			object_n[i].x = object[i].x - extrinsics->x;
			object_n[i].y = object[i].y - extrinsics->y;
			object_n[i].z = object[i].z - extrinsics->z;
			lengths[i] = pnp_la_normalize3_p(&object_n[i]);
			//lengths[i] = sqrt(pnp_la_dot3_p(&object_n[i], &object_n[i]));
		}

		/* calculating objective function and its jacobian */
		for(k = 0, i = 0; i < n; i++) {
			for(j = i + 1; j < n; j++, k++) {
				cos_AB = pnp_la_dot3_p(&object_n[i], &object_n[j]); 
				
				f[k] = cos_ab[k] - cos_AB;
				
				J_IDX(k, 0) =
					object_n[i].x * cos_AB / lengths[i] + 
					object_n[j].x * cos_AB / lengths[j] -
					object_n[i].x / lengths[j] - object_n[j].x / lengths[i];
				J_IDX(k, 1) =
					object_n[i].y * cos_AB / lengths[i] +
					object_n[j].y * cos_AB / lengths[j] -
					object_n[i].y / lengths[j] - object_n[j].y / lengths[i];
				J_IDX(k, 2) =
					object_n[i].z * cos_AB / lengths[i] +
					object_n[j].z * cos_AB / lengths[j] -
					object_n[i].z / lengths[j] - object_n[j].z / lengths[i];
			}
		}
		
		/* converting overdeterminated system to normal equations */
		memset(A, 0, sizeof(double) * 9);
		memset(JT_f, 0, sizeof(double) * 3);

		for(i = 0; i < comb; i++) {
			A[0][0] += J_IDX(i, 0) * J_IDX(i, 0);
			A[0][0] += J_IDX(i, 0) * J_IDX(i, 1);
			A[0][2] += J_IDX(i, 0) * J_IDX(i, 2);
			
			A[1][0] += J_IDX(i, 1) * J_IDX(i, 0);
			A[1][1] += J_IDX(i, 1) * J_IDX(i, 1);
			A[1][2] += J_IDX(i, 1) * J_IDX(i, 2);
			
			A[2][0] += J_IDX(i, 2) * J_IDX(i, 0);
			A[2][1] += J_IDX(i, 2) * J_IDX(i, 1);
			A[2][2] += J_IDX(i, 2) * J_IDX(i, 2);
	
			JT_f[0] += J_IDX(i, 0) * f[i];
			JT_f[1] += J_IDX(i, 1) * f[i];
			JT_f[2] += J_IDX(i, 2) * f[i];
		}

		/* calculating deltas */
		if(pnp_la_solven(A, JT_f, 3, tmp)) goto pnp_iter_church_end;
		
		extrinsics->x += JT_f[0];
		extrinsics->y += JT_f[1];
		extrinsics->z += JT_f[2];
		
		error = pnp_la_dot3(JT_f, JT_f);
		
		error_min = (error < error_min) ? error : error_min;
		error_max = (error > error_max) ? error : error_max;

		if(error > gn->error_max) goto pnp_iter_church_end;

		printf("[pnp_iter_church] iterations %d, error=%lf\n", iter, error); 
	}
	
	gn->iterations = iter;
	gn->error_min = error;
	
	/* calculating rotation matrix */

	memset(A, 0, sizeof(double) * 9);
	for(i = 0; i < n; i++) {
		A[0][0] += image_n[i].x * image_n[i].x;
		A[0][1] += image_n[i].x * image_n[i].y;
		A[0][2] += image_n[i].x * image_n[i].z;
	
		A[1][0] += image_n[i].y * image_n[i].x;
		A[1][1] += image_n[i].y * image_n[i].y;
		A[1][2] += image_n[i].y * image_n[i].z;

		A[2][0] += image_n[i].z * image_n[i].x;
		A[2][1] += image_n[i].z * image_n[i].y;
		A[2][2] += image_n[i].z * image_n[i].z;
	}

	if(pnp_la_inv3(A, A_inv)) goto pnp_iter_church_end;

	for(i = 0; i < n; i++) {
		A[0][0] += object_n[i].x * object_n[i].x;
		A[0][1] += object_n[i].x * object_n[i].y;
		A[0][2] += object_n[i].x * object_n[i].z;
	
		A[1][0] += object_n[i].y * object_n[i].x;
		A[1][1] += object_n[i].y * object_n[i].y;
		A[1][2] += object_n[i].y * object_n[i].z;

		A[2][0] += object_n[i].z * object_n[i].x;
		A[2][1] += object_n[i].z * object_n[i].y;
		A[2][2] += object_n[i].z * object_n[i].z;
	}

	pnp_la_mul_3333(A, A_inv, extrinsics->R);

	res = 0;

pnp_iter_church_end:
	if(image_n) free(image_n);
	if(object_n) free(object_n);
	if(cos_ab) free(cos_ab);
	if(lengths) free(lengths);
	if(f) free(f);
	if(J) free(J);

	gn->iterations = iter;
	if(iter > 0) {
		gn->error_min = error_min;
		gn->error_max = error_max;
	} else {
		gn->error_min = 0;
		gn->error_max = 0;
	}

	return res;

#undef J_IDX
}

int pnp_iter_collinearity(pnp_intristics_t * intristics, pnp_point2_t image[], pnp_point3_t object[], int n, pnp_gauss_newton_params_t * gn, pnp_extrinsics_t * extrinsics) {
#define J_IDX(i, j) J[(i) * 6 + (j)]

	int iter = 0, i, j, res = -1;
	
	double R[3][3], A[6][6], JT_f[6], error = gn->error_max;

	double * J = NULL, * f = NULL;
	pnp_point2_t * img_c = NULL;

	J = malloc(sizeof(double) * n * 6);
	f = malloc(sizeof(double) * n * 2);
	img_c = malloc(sizeof(pnp_point2_t) * n);
	
	if(!J || !f || !img_c) goto pnp_iter_collinearity_end;

	for(i = 0; i < n; i++) {
		img_c[i].x = (image[i].x - intristics->x0) * intristics->fx_i;
		img_c[i].y = (image[i].y - intristics->y0) * intristics->fy_i; 
	}
	struct {
		double omega, phi, kappa;
	} r;

	pnp_dcm_zyx_to_euler(extrinsics->R, (double *)&r); 

	for(iter = 0; iter < gn->iterations && error > gn->error_min; iter++) {
		
		double dX, dY, dZ, Fx, Fy, Fz;
	}

	res = 0;

pnp_iter_collinearity_end:
	if(J) free(J);
	if(f) free(f);
	if(img_c) free(img_c);

	return res;

#undef J_IDX
}
