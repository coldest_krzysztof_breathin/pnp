#ifndef __PNP_LINALG_H__
#define __PNP_LINALG_H_

#include "types.h"

double pnp_la_dot3(double [3], double [3]);
double pnp_la_dot3_p(pnp_point3_t *, pnp_point3_t *);
void pnp_la_cross(double [3], double [3], double [3]);
double pnp_la_normalize3_p(pnp_point3_t *);
int pnp_la_inv3(double [3][3], double [3][3]);
void pnp_la_mul_3333(double [3][3], double [3][3], double [3][3]);
void pnp_la_mul_3331(double [3][3], double [3], double [3]);
int pnp_la_solven(double *, double *, int, double *);

#endif
