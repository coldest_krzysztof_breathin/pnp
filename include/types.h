#ifndef __PNP_TYPES_H__
#define __PNP_TYPES_H__

#define _PNP_LA_EPSILON_ 1e-15

typedef struct {
	double fx, fy;
	double fx_i, fy_i;

	double x0, y0;
	double x0_i, y0_i;

	double x0_fx_i;
	double y0_fy_i;

	double k1, k2, k3;
	double p1, p2;

} pnp_intristics_t;

typedef struct {
	double x, y, z;
	double R[3][3];
} pnp_extrinsics_t;

typedef struct {
	double x, y;
} pnp_point2_t;

typedef struct {
	double x, y, z;
} pnp_point3_t;

typedef struct {
	int iterations;
	double error_min;
	double error_max;
} pnp_gauss_newton_params_t;

#endif
