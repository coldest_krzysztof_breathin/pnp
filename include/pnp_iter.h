#ifndef __PNP_ITER_H__
#define __PNP_ITER_H__

#include "types.h"

int pnp_iter_church(pnp_intristics_t *, pnp_point2_t [], pnp_point3_t [], int, pnp_gauss_newton_params_t *, pnp_extrinsics_t *);
int pnp_iter_collinearity(pnp_intristics_t *, pnp_point2_t [], pnp_point3_t [], int, pnp_gauss_newton_params_t *, pnp_extrinsics_t *);

#endif
