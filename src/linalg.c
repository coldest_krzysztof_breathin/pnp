#include <math.h>

#include "types.h"

double pnp_la_dot3(double vector_1[3], double vector_2[3]) {
	return vector_1[0] * vector_2[0] + vector_1[1] * vector_2[1] + vector_1[2] * vector_2[2];
}

double pnp_la_dot3_p(pnp_point3_t * point_1, pnp_point3_t * point_2) {
	return point_1->x * point_2->x + point_1->y * point_2->y + point_1->z * point_2->z;
}

void pnp_la_cross(double vector_1[3], double vector_2[3], double vector_out[3]) {
	vector_out[0] = vector_1[1] * vector_2[2] - vector_1[2] * vector_2[1];
	vector_out[1] = vector_1[2] * vector_2[0] - vector_1[0] * vector_2[2];
	vector_out[2] = vector_1[0] * vector_2[1] - vector_1[1] * vector_2[0];
}

double pnp_la_normalize3_p(pnp_point3_t * point) {
	double length = sqrt(pnp_la_dot3_p(point, point));

	point->x /= length;
	point->y /= length;
	point->z /= length;

	return length;
}
int pnp_la_inv3(double matrix[3][3], double inversed[3][3]) {
	double det;

	inversed[0][0] = (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]);
	inversed[1][0] = (matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2]);
	inversed[2][0] = (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);

	det = matrix[0][0] * inversed[0][0] + matrix[0][1] * inversed[1][0] + matrix[0][2] * inversed[2][0];

	if(fabs(det) < _PNP_LA_EPSILON_) return -1;
	
	inversed[0][0] /= det;
	inversed[0][1] = (matrix[0][2] * matrix[2][1] - matrix[0][1] * matrix[2][2]) / det;
	inversed[0][2] = (matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1]) / det;
	inversed[1][0] /= det;
	inversed[1][1] = (matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0]) / det;
	inversed[1][2] = (matrix[0][2] * matrix[1][0] - matrix[0][0] * matrix[1][2]) / det;
	inversed[2][0] /= det;
	inversed[2][1] = (matrix[0][1] * matrix[2][0] - matrix[0][0] * matrix[2][1]) / det;
	inversed[2][2] = (matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]) / det;

	return 0;
}

void pnp_la_mul_3333(const double matrix_1[3][3], const double matrix_2[3][3], double out[3][3]) {
	out[0][0] = matrix_1[0][0] * matrix_2[0][0] + matrix_1[0][1] * matrix_2[1][0] + matrix_1[0][2] * matrix_2[2][0];
	out[0][1] = matrix_1[0][0] * matrix_2[0][1] + matrix_1[0][1] * matrix_2[1][1] + matrix_1[0][2] * matrix_2[2][1];
	out[0][2] = matrix_1[0][0] * matrix_2[0][2] + matrix_1[0][1] * matrix_2[1][2] + matrix_1[0][2] * matrix_2[2][2];
	
	out[1][0] = matrix_1[1][0] * matrix_2[0][0] + matrix_1[1][1] * matrix_2[1][0] + matrix_1[1][2] * matrix_2[2][0];
	out[1][1] = matrix_1[1][0] * matrix_2[0][1] + matrix_1[1][1] * matrix_2[1][1] + matrix_1[1][2] * matrix_2[2][1];
	out[1][2] = matrix_1[1][0] * matrix_2[0][2] + matrix_1[1][1] * matrix_2[1][2] + matrix_1[1][2] * matrix_2[2][2];
	
	out[2][0] = matrix_1[2][0] * matrix_2[0][0] + matrix_1[2][1] * matrix_2[1][0] + matrix_1[2][2] * matrix_2[2][0];
	out[2][1] = matrix_1[2][0] * matrix_2[0][1] + matrix_1[2][1] * matrix_2[1][1] + matrix_1[2][2] * matrix_2[2][1];
	out[2][2] = matrix_1[2][0] * matrix_2[0][2] + matrix_1[2][1] * matrix_2[1][2] + matrix_1[2][2] * matrix_2[2][2];
}

void pnp_la_mul_3331(const double matrix_1[3][3], const double matrix_2[3], double out[3]) {
	out[0] = matrix_1[0][0] * matrix_2[0] + matrix_1[0][1] * matrix_2[1] + matrix_1[0][2] * matrix_2[2];
	out[1] = matrix_1[1][0] * matrix_2[0] + matrix_1[1][1] * matrix_2[1] + matrix_1[1][2] * matrix_2[2];
	out[2] = matrix_1[2][0] * matrix_2[0] + matrix_1[2][1] * matrix_2[1] + matrix_1[2][2] * matrix_2[2];
}

static inline void pnp_la_solven_swap(double * ptr_1, double * ptr_2, int size, double * buffer) {
	memcpy(buffer, ptr_1, size);
	memcpy(ptr_1, ptr_2, size);
	memcpy(ptr_2, buffer, size);
}

int pnp_la_solven(double * A, double * b, int n, double * swap) {
#define A_IDX(i, j) A[(i) * n + (j)]
	int i, j, k;
	double tmp;
	const int row_size = sizeof(double) * n;

	for(i = 0; i < n; i++) {
		tmp = A_IDX(i, i);
		
		/* partial pivoting */		
		if(fabs(tmp) < _PNP_LA_EPSILON_) {
			for(j = i + 1; j < n && fabs(tmp = A_IDX(j, i)) < _PNP_LA_EPSILON_; j++);
			if(j == n) return -1;
			else {
				pnp_la_solven_swap(&A_IDX(i, 0), &A_IDX(j, 0), row_size, swap);
				pnp_la_solven_swap(&b[i], &b[j], sizeof(double), swap);
			}
		}

		/* elimination */
		for(j = 0; j < n; j++) A_IDX(i, j) /= tmp;	
		b[i] /= tmp;

		for(j = 0; j < n; j++) {
			if(i == j) continue;
			
			tmp = A_IDX(j, i);

			for(k = 0; k < n; k++) A_IDX(j, k) -= tmp * A_IDX(i, k);
			b[j] -= tmp * b[i];
		}		
	}
	
	return 0;

#undef A_IDX
}

